<?php
  set_include_path(dirname(__FILE__));
  ini_set('display_errors', 1);
  error_reporting(E_ALL);

  session_start();

  if (isset($_GET['subject'])) {$subject=$_GET['subject'];$_SESSION['subject']=$subject;}
  else {
    if (isset($_SESSION['subject'])) {$subject=$_SESSION['subject'];}
    else {$subject="anonymous";};
  }
  if (isset($_GET['condnum'])) {$condnum=$_GET['condnum'];}
  else {
    if (isset($_SESSION['condnum'])) {$condnum=$_SESSION['condnum'];$_SESSION['condnum']=$condnum;}
    else {$condnum=-1;};
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Christoph Trattner">

    <title>Food recommendation and salt</title><!-- page title -->

    <script src="js/jquery-2.1.0.min.js"></script>
    <link rel="stylesheet" href="css/jquery.typeahead.css">
    <script src="js/jquery.typeahead.js"></script>

    <script src ="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script language=javascript src="mlweb20.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

  </head>
  <body onLoad="timefunction('onload', 'body', 'body')">
    <!--BEGIN set vars-->
    <script language="javascript">
      //override defaults
      mlweb_outtype = "CSV";
      mlweb_fname = "mlwebform";
      chkFrm = false;
      warningTxt = "Please answer all questions.";
      choice = "";
    </script>

    <form id="mlwebform" name="mlwebform" onSubmit="return checkForm(this)" method="POST" action="save.php">
      <input type=hidden id='processData' name="procdata" value="">
      <!-- set all variables here -->
      <input id="expName" type=hidden name="expname" value="questionnaire">
      <input type=hidden name="nextURL" value="3DD957D9.html">
      <input type=hidden name="to_email" value="">
      <!--these will be set by the script -->
      <input type=hidden name="subject" value="<?php echo($subject)?>">
      <input type=hidden id="condnum" name="condnum" value="<?php echo($condnum)?>">
      <input id="choice" type=hidden name="choice" value="">

      <div class="container">
        <div class="cont mt-5">
          <div class="card-body">
            <h3 class="card-title">Questionnaire</h3>
            <div class="card-content">
              <div class="row">
                <div class="col">
                  <p>Do you like garlic?</p>
                </div>
                <div class="col">
                  <input class="w3-radio" type="radio" name="Rec-q1" value="yes" required>  Yes
                  <input class="w3-radio" type="radio" name="Rec-q1" value="no" required>  No
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <p>Do you like oregano?</p>
                </div>
                <div class="col">
                  <input class="w3-radio" type="radio" name="Rec-q2" value="yes" required>  Yes
                  <input class="w3-radio" type="radio" name="Rec-q2" value="no" required>  No
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <p>Do you like rosemary?</p>
                </div>
                <div class="col">
                  <input class="w3-radio" type="radio" name="Rec-q3" value="yes" required>  Yes
                  <input class="w3-radio" type="radio" name="Rec-q3" value="no" required>  No
                </div>
              </div>
              <!--
              <div class="row">
                <div class="col mt-4 ml-1">
                  <p>Are you most familiar with U.S. Standard (e.g. pounds) or metric (e.g. kilograms) measures?</p>
                    <input class="w3-radio" type="radio" name="Rec-q4" value="imperial" required> U.S. Standard
                    <input class="w3-radio" type="radio" name="Rec-q4" value="metric" required> Metric
                </div>
              </div>

              -->

              <div class="row mt-4 ml-1 mr-4">
                <p>What is your salty taste preference on a scale from 0 (no salty taste) to 10 (very salty taste)?</p>
                <div class="row">
                  <div class="col">
                    <input class="w3-radio" type="radio" name="Rec-q4" value="0" required> 0
                    <input class="w3-radio" type="radio" name="Rec-q4" value="1" required> 1
                    <input class="w3-radio" type="radio" name="Rec-q4" value="2" required> 2
                    <input class="w3-radio" type="radio" name="Rec-q4" value="3" required> 3
                    <input class="w3-radio" type="radio" name="Rec-q4" value="4" required> 4
                    <input class="w3-radio" type="radio" name="Rec-q4" value="5" required> 5
                    <input class="w3-radio" type="radio" name="Rec-q4" value="6" required> 6
                    <input class="w3-radio" type="radio" name="Rec-q4" value="7" required> 7
                    <input class="w3-radio" type="radio" name="Rec-q4" value="8" required> 8
                    <input class="w3-radio" type="radio" name="Rec-q4" value="9" required> 9
                    <input class="w3-radio" type="radio" name="Rec-q4" value="10" required> 10
                  </div>
                </div>
              </div>


              <div class="row mt-4 ml-1 mr-4">
                <p>Research shows that many people can be just as satisfied if salt is reduced a little and garlic,
                oregano or rosemary added. To what extent do you think that you would be satisfied with this in the future on a scale
                from 1 (to no extent) to 7 (to a high extent)?</p>
                <div class="row">
                  <div class="col">
                    <input class="w3-radio" type="radio" name="Rec-q5" value="1" required> 1
                    <input class="w3-radio" type="radio" name="Rec-q5" value="2" required> 2
                    <input class="w3-radio" type="radio" name="Rec-q5" value="3" required> 3
                    <input class="w3-radio" type="radio" name="Rec-q5" value="4" required> 4
                    <input class="w3-radio" type="radio" name="Rec-q5" value="5" required> 5
                    <input class="w3-radio" type="radio" name="Rec-q5" value="6" required> 6
                    <input class="w3-radio" type="radio" name="Rec-q5" value="7" required> 7
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col text-center">
                  <button class="confirm text-center btn btn-primary mt-3" name="submit" value="confirm">Next</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </body>
<html>

<script type="text/javascript">

  o=$("#condnum").val();
  if (o<0) {o="random"};
  $(document).ready(function () {
  $(".confirm").click(function (event) {
    if (choice=="" && $(".choiceButton").length>0) {event.preventDefault();return false;}
    });
  });

</script>
