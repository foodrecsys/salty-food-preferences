<?php
  set_include_path(dirname(__FILE__));
  ini_set('display_errors', 1);
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  error_reporting(E_ALL);

  session_start();

  if (isset($_GET['subject'])) {$subject=$_GET['subject'];$_SESSION['subject']=$subject;}
  else {
    if (isset($_SESSION['subject'])) {$subject=$_SESSION['subject'];}
    else {$subject="anonymous";};
  }
  if (isset($_GET['condnum'])) {$condnum=$_GET['condnum'];}
  else {
    if (isset($_SESSION['condnum'])) {$condnum=$_SESSION['condnum'];$_SESSION['condnum']=$condnum;}
    else {$condnum=-1;};
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Christoph Trattner">

    <title>Food recommendation and salt</title><!-- page title -->

    <script src="js/jquery-2.1.0.min.js"></script>
    <link rel="stylesheet" href="css/jquery.typeahead.css">
    <script src="js/jquery.typeahead.js"></script>

    <script src ="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script language=javascript src="mlweb20.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

    <script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-34429356-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();

    </script>
  </head>
  <body onLoad="timefunction('onload', 'body', 'body')">
    <!--BEGIN set vars-->
    <script language="javascript">
      //override defaults
      mlweb_outtype = "CSV";
      mlweb_fname = "mlwebform";
      chkFrm = false;
      warningTxt = "Please answer all questions.";
      choice = "";
    </script>

    <form id="mlwebform" name="mlwebform" onSubmit="return checkForm(this)" method="POST" action="save.php">
      <input type=hidden id='processData' name="procdata" value="">
      <!-- set all variables here -->
      <input id="expName" type=hidden name="expname" value="index7">
      <input type=hidden name="nextURL" value="index8.php">
      <input type=hidden name="to_email" value="">
      <!--these will be set by the script -->
      <input type=hidden name="subject" value="<?php echo($subject)?>">
      <input type=hidden id="condnum" name="condnum" value="<?php echo($condnum)?>">
      <input id="choice" type=hidden name="choice" value="">

      <?php
        $data = null;
        $file = fopen('visual_data_selection.csv', 'r');
        while (($line = fgetcsv($file, 0, "\t")) !== FALSE) {
          $dat = array();
          for ($i=0; $i < count($line);$i++) {
              array_push($dat,$line[$i]);
          }
          $data[$line[0]] = $dat;
        }


        $sort_s = array();
        $data_s = $data;
        foreach ($data_s as $key => $row) {
            $sort_s[$key] = $row[3];
        }
        array_multisort($sort_s, SORT_DESC, $data_s);

        $sort_e = array();
        $data_e = $data;
        foreach ($data_e as $key => $row) {
            $sort_e[$key] = $row[4];
        }
        array_multisort($sort_e, SORT_DESC, $data_e);


        $data_s_new = array();
        foreach ($data_s as $key => $row) {
          if ($_SESSION['c2qt'] == "Chicken"){
            if ($row[12] == 1) {
              array_push($data_s_new, $row);
            }
          }
          else {
            if ($row[11] == 1) {
              array_push($data_s_new, $row);
            }
          }
        }


        $data_e_new = array();
        foreach ($data_e as $key => $row) {
          if ($_SESSION['c2qt'] == "Chicken"){
            if ($row[12] == 1) {
              array_push($data_e_new, $row);
            }
          }
          else {
            if ($row[11] == 1) {
              array_push($data_e_new, $row);
            }
          }
        }


        $data_new = array();
        if ($_SESSION['c2q'] == 'cond1') {
          $data_new = $data_s_new;
          $tastemap = round($data_new[0][3], 1)*10;
        }
        elseif ($_SESSION['c2q'] == 'cond2') {
          $data_new = $data_e_new;
          $tastemap = round($data_new[0][4], 1)*10;
        }
      ?>

      <div id="debugging">
        <?php
          include 'debugging.php';
         ?>
      </div>

      <div class="container">
        <div class="toptext">
          <!--h2>Term: <?php echo($_SESSION['c2qt']);?></h2-->
          <p>Please read the presentation below and score the recipe according to how it fulfills your salty taste preference on a scale from 1 to 7 (1: Very Poorly, 7: Very Well)</p>
          <div class="nav">
            <div class="likert">
              <input class="w3-radio" type="radio" name="Rec-q1" value="1" required> 1
              <input class="w3-radio" type="radio" name="Rec-q1" value="2" required> 2
              <input class="w3-radio" type="radio" name="Rec-q1" value="3" required> 3
              <input class="w3-radio" type="radio" name="Rec-q1" value="4" required> 4
              <input class="w3-radio" type="radio" name="Rec-q1" value="5" required> 5
              <input class="w3-radio" type="radio" name="Rec-q1" value="6" required> 6
              <input class="w3-radio" type="radio" name="Rec-q1" value="7" required> 7
            </div>
          </div>
          <input type='hidden' name='cond' value='<?php echo $_SESSION['c2q'];?>'/>
          <input type='hidden' name='query' value='<?php echo $_SESSION['c2qt'];?>'/>
          <input type='hidden' name='ID' value='<?php echo $data_new[0][0];?>'/>
          <button class="confirm nextBTN btn btn-primary" name="submit" value="confirm">Next</button>
        </div>
        <div class="cont">
          <div class="card-body">
            <h3 class="card-title"><?php echo $data_new[0][1]; ?></h3>
            <div class="row mb-4 align-items-start">
              <div class="col">
                <img class="card-img-top" src="<?php echo $data_new[0][17]; ?>" alt="">
              </div>
              <div class="col">
                <h5>Calculated Taste</h5>
                <img class="tastemap" src="tastemaps/taste-map-<?php echo $tastemap ?>.svg" alt="">
                <?php
                  if ($data_new[0][5] !== '' && $_SESSION['c2q'] == 'cond2') {
                    echo '<h5>Healthy ingredients enhancing salty taste</h5>';
                    echo '<p class="expl">';
                    echo $data_new[0][5];
                    echo '</p>';
                  }
                ?>
              </div>
            </div>
            <div class="card-content">
              <h5>Ingredients</h5>
              <ul class="indented"><?php echo $data_new[0][6]; ?></ul>
              <h5>Directions</h5>
              <div class="indented"><?php echo $data_new[0][8]; ?></div>
              <h5>Nutrition facts (per serving)</h5>
              <div class="indented"><?php echo $data_new[0][9]; ?></div>
              <!--div class="bottomlikert btn-toolbar" role="toolbar" aria-label="..."-->
            </div>
          </div>
        </div>
      </div>
    </form>
  </body>
<html>

<script type="text/javascript">
  o=$("#condnum").val();
  if (o<0) {o="random"};
  $(document).ready(function () {
  $(".confirm").click(function (event) {
    if (choice=="" && $(".choiceButton").length>0) {event.preventDefault();return false;}
    });
  });
</script>
