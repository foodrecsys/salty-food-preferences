<?php
  set_include_path(dirname(__FILE__));
  ini_set('display_errors', 1);
  error_reporting(E_ALL);

  session_start();

  if (isset($_GET['subject'])) {$subject=$_GET['subject'];$_SESSION['subject']=$subject;}
  else {
    if (isset($_SESSION['subject'])) {$subject=$_SESSION['subject'];}
    else {$subject="anonymous";};
  }
  if (isset($_GET['condnum'])) {$condnum=$_GET['condnum'];}
  else {
    if (isset($_SESSION['condnum'])) {$condnum=$_SESSION['condnum'];$_SESSION['condnum']=$condnum;}
    else {$condnum=-1;};
  }
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Christoph Trattner">

    <title>Food recommendation and salt</title><!-- page title -->

    <script src="js/jquery-2.1.0.min.js"></script>
    <link rel="stylesheet" href="css/jquery.typeahead.css">
    <script src="js/jquery.typeahead.js"></script>

    <script src ="js/tether.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script language=javascript src="mlweb20.js"></script>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">

  </head>
  <body onLoad="timefunction('onload', 'body', 'body')">
    <!--BEGIN set vars-->
    <script language="javascript">
      //override defaults
      mlweb_outtype = "CSV";
      mlweb_fname = "mlwebform";
      chkFrm = false;
      warningTxt = "Please answer all questions.";
      choice = "";
    </script>

    <form id="mlwebform" name="mlwebform" onSubmit="return checkForm(this)" method="POST" action="save.php">
      <input type=hidden id='processData' name="procdata" value="">
      <!-- set all variables here -->
      <input id="expName" type=hidden name="expname" value="query2">
      <input type=hidden name="nextURL" value="index7.php">
      <input type=hidden name="to_email" value="">
      <!--these will be set by the script -->
      <input type=hidden name="subject" value="<?php echo($subject)?>">
      <input type=hidden id="condnum" name="condnum" value="<?php echo($condnum)?>">
      <input id="choice" type=hidden name="choice" value="">

      <?php
        $data = null;
        $file = fopen('visual_data_selection.csv', 'r');
        while (($line = fgetcsv($file, 0, "\t")) !== FALSE) {
          $dat = array();
          for ($i=0; $i < count($line);$i++) {
              array_push($dat,$line[$i]);
          }
          $data[$line[0]] = $dat;
        }


        $sort_s = array();
        $data_s = $data;
        foreach ($data_s as $key => $row) {
            $sort_s[$key] = $row[3];
        }
        array_multisort($sort_s, SORT_DESC, $data_s);

        $sort_e = array();
        $data_e = $data;
        foreach ($data_e as $key => $row) {
            $sort_e[$key] = $row[4];
        }
        array_multisort($sort_e, SORT_DESC, $data_e);


        $data_s_new = array();
        foreach ($data_s as $key => $row) {
          if ($_SESSION['c2qt'] == "Chicken"){
            if ($row[12] == 1) {
              array_push($data_s_new, $row);
            }
          }
          else {
            if ($row[11] == 1) {
              array_push($data_s_new, $row);
            }
          }
        }


        $data_e_new = array();
        foreach ($data_e as $key => $row) {
          if ($_SESSION['c2qt'] == "Chicken"){
            if ($row[12] == 1) {
              array_push($data_e_new, $row);
            }
          }
          else {
            if ($row[11] == 1) {
              array_push($data_e_new, $row);
            }
          }
        }


        $data_new = array();
        if ($_SESSION['c2q'] == 'cond1') {
          $data_new = $data_s_new;
          $tastemap = round($data_new[0][3], 1)*10;
        }
        elseif ($_SESSION['c2q'] == 'cond2') {
          $data_new = $data_e_new;
          $tastemap = round($data_new[0][4], 1)*10;
        }
      ?>

      <div id="debugging">
        <?php
          include 'debugging.php';
         ?>
      </div>

      <div class="container">
        <div class="cont mt-5">
          <div class="card-body">
            <h3 class="card-title">Search results</h3>
            <div class="card-content">
              <p>Here is another example of how a search in our Recipe site can look. On the next pages you will go through the six search results and judge them according to <b>how well it fulfills your salty taste preference</b>.</p>
              <div class="row">
                <div class="col">
                  <input type="text" value="<?php echo $_SESSION['c2qt']?>" class="form-control" id="search" placeholder="<?php echo $_SESSION['c2qt']?>" name="search" readonly>
                </div>
              </div>
              <ul class="list-group results">
                <li class="list-group-item">
                  <img class="thumb" src="<?php echo $data_new[0][17]; ?>" alt="">
                  <h4 class="txtresult"><?php echo $data_new[0][1] ?></h4>
                  <div class="col">
                    <img class="right" src="tastemaps/taste-map-mini-<?php echo round($data_new[0][4], 1)*10; ?>.svg" alt="">
                  </div>
                </li>
                <li class="list-group-item">
                  <img class="thumb" src="<?php echo $data_new[1][17]; ?>" alt="">
                  <h4 class="txtresult"><?php echo $data_new[1][1] ?></h4>
                  <div class="col">
                    <img class="right" src="tastemaps/taste-map-mini-<?php echo round($data_new[1][4], 1)*10; ?>.svg" alt="">
                  </div>
                </li>
                <li class="list-group-item">
                  <img class="thumb" src="<?php echo $data_new[2][17]; ?>" alt="">
                  <h4 class="txtresult"><?php echo $data_new[2][1] ?></h4>
                  <div class="col">
                    <img class="right" src="tastemaps/taste-map-mini-<?php echo round($data_new[2][4], 1)*10; ?>.svg" alt="">
                  </div>
                </li>
                <li class="list-group-item">
                  <img class="thumb" src="<?php echo $data_new[3][17]; ?>" alt="">
                  <h4 class="txtresult"><?php echo $data_new[3][1] ?></h4>
                  <div class="col">
                    <img class="right" src="tastemaps/taste-map-mini-<?php echo round($data_new[3][4], 1)*10; ?>.svg" alt="">
                  </div>
                </li>
                <li class="list-group-item">
                  <img class="thumb" src="<?php echo $data_new[4][17]; ?>" alt="">
                  <h4 class="txtresult"><?php echo $data_new[4][1] ?></h4>
                  <div class="col">
                    <img class="right" src="tastemaps/taste-map-mini-<?php echo round($data_new[4][4], 1)*10; ?>.svg" alt="">
                  </div>
                </li>
                <li class="list-group-item">
                  <img class="thumb" src="<?php echo $data_new[5][17]; ?>" alt="">
                  <h4 class="txtresult"><?php echo $data_new[5][1] ?></h4>
                  <div class="col">
                    <img class="right" src="tastemaps/taste-map-mini-<?php echo round($data_new[5][4], 1)*10; ?>.svg" alt="">
                  </div>
                </li>
              </ul>
              <div class="row">
                <div class="col text-center">
                  <button class="confirm text-center btn btn-primary mt-3" name="submit" value="confirm">Next</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </body>
<html>

<script type="text/javascript">
  o=$("#condnum").val();
  if (o<0) {o="random"};
  $(document).ready(function () {
  $(".confirm").click(function (event) {
    if (choice=="" && $(".choiceButton").length>0) {event.preventDefault();return false;}
    });
  });
</script>
