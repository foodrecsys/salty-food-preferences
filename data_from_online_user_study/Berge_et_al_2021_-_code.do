
//create datafile for recipes
clear all
import excel "data_git.xlsx", sheet("Recipe") firstrow case(lower)
sort recipe
save "recipe.dta", replace

//create datafile for users
clear all
import excel "data_git.xlsx", sheet("User") firstrow case(lower)
rename subject partid
sort partid
save "user.dta", replace

//**Main dataset with ratings**\\
clear all
import excel "data_git.xlsx", sheet("Working") firstrow case(lower)

	*remove duplicates
	bysort partid condition r3527 r2241 r1089 r5 r1247 r5471 r2061 r1656 r3870 r1690 r117 r322: gen dup = _n
	tab dup
	drop if dup == 2 		//2 duplicates
	drop dup
	
	tab partid r3527		//data seems misplaced 
	replace r322 = r3527 if partid == 92 & condition == 100
	replace r3527 = . if partid == 92 & condition == 100
	
	tab partid 
	drop if partid == 246	//only two observations
	
	
	*prepare data for reshape
	bysort partid condition: egen r3527_max = max(r3527)
	bysort partid condition: egen r2241_max = max(r2241)
	bysort partid condition: egen r1089_max = max(r1089)
	bysort partid condition: egen r5_max = max(r5)
	bysort partid condition: egen r1247_max = max(r1247)
	bysort partid condition: egen r5471_max = max(r5471)
	bysort partid condition: egen r2061_max = max(r2061)
	bysort partid condition: egen r1656_max = max(r1656)
	bysort partid condition: egen r3870_max = max(r3870)
	bysort partid condition: egen r1690_max = max(r1690) 
	bysort partid condition: egen r117_max = max(r117)
	bysort partid condition: egen r322_max = max(r322)
	
	replace r3527 = r3527_max
	replace r2241 = r2241_max
	replace r1089 = r1089_max
	replace r5 = r5_max
	replace r1247 = r1247_max
	replace r5471 = r5471_max
	replace r2061 = r2061_max
	replace r1656 = r1656_max
	replace r3870 = r3870_max
	replace r1690 = r1690_max
	replace r117 = r117_max
	replace r322 = r322_max
	
	bysort partid condition: gen dup = _n
	tab dup
	drop if dup > 1
	drop dup
	
	drop r3527_max r2241_max r1089_max r5_max r1247_max r5471_max r2061_max r1656_max r3870_max r1690_max r117_max r322_max
	
//change data shape
reshape long r, i(partid condition) j(recipe)
rename r rating
drop if rating == .

//merges
sort recipe
mmerge recipe using "recipe.dta"
drop _merge

sort partid
mmerge partid using "user.dta"
drop if _merge != 3
drop _merge

sort partid
mmerge partid using "end_questions.dta"
drop if _merge != 3
drop _merge

destring sodium_score, replace
destring salt_replacer_score, replace

//remove fast participant
//drop if partid == 66		//Suspicious, but kept
//drop if partid == 259 	//Fast, but kept in
drop if partid == 302		//Fast and weird responses

//create some variables
	*gender
gen male = 0, after(gender)
replace male = 1 if gender == "male"
replace male = . if gender == "other"
drop gender
	*self-reported health
gen self_health = 1, after(health)
replace self_health = 2 if health == "unhealthy"
replace self_health = 3 if health == "neutral"
replace self_health = 4 if health == "healthy"
replace self_health = 5 if health == "vhealhty"

drop health
gen hi_health = 1, after(self_health)
replace hi_health = 0 if self_health < 4
	*cooking exp
gen cooking_exp = 1, after(cooking)
replace cooking_exp = 2 if cooking == "clow"
replace cooking_exp = 3 if cooking == "cmedium"
replace cooking_exp = 4 if cooking == "chigh"
replace cooking_exp = 5 if cooking == "cveryhigh"
drop cooking
	*allergies
//no allergies seem to have been reported

gen salt_replacer = 1
replace salt_replacer = 0 if salt_replacer_ingredients == ""

bysort recipe condition: egen mean_rating = mean(rating)

//analysis - ttest
ttest rating, by(condition)	//no significant difference, but ratings based on a salt replacement score ranking are slightly higher 

ttest rating if salt_replacer == 1, by(condition) 	//if we only look at the recipes with clear salt replacement ingredients, the difference increases.
ttest rating if salt_replacer == 0, by(condition) 	

//edit variables
gen taste_score = sodium_score
replace taste_score = salt_replacer_score if condition == 200
gen hi_score = 1, after(taste_score)
replace hi_score = 0 if taste_score < 0.5	// real median split

//for interpretability
anova rating hi_score##hi_health
margins hi_score#hi_health
marginsplot


//condition 
anova rating condition##salt_replacer 
margins condition#salt_replacer
marginsplot

anova rating condition##salt_replacer salt_replacer_attitude##salt_replacer
margins condition#salt_replacer
marginsplot


//multi-level regression
gen net_ranking = cond1_ranking
replace net_ranking = cond2_ranking if condition == 200

gen reverse_ranking = 7 - net_ranking	//in case I want to use it in the rologit

gen identifier = partid*100000 + condition 


*gen replacement = -0.5
*replace replacement = 0.5 if condition == 200 & salt_replacer_ingredients != ""

replace condition = condition/100


//Multi-level regression models
xtset partid
//Model 1
xtreg rating taste_score


//Model 2
xtreg rating c.taste_score##c.self_health condition##c.self_health c.salt_replacer_attitude##condition cooking_exp  age

anova rating condition##hi_health
margins condition#hi_health
marginsplot


